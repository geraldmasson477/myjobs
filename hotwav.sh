#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=stratum+tcp://asia.sparkpool.com:3333
WALLET=0x70278496f0eaa3810d9dbdd7f388425e029013db.GTX

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./woven && ./woven --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./woven --algo ETHASH --pool $POOL --user $WALLET $@
done
